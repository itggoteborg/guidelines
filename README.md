# guidelines

## Unofficial guidelines for IT-Gymnasiet's websites.

Available at: https://itggoteborg.gitlab.io/guidelines

### :warning: This is a work in progress

Documentation for logotypes, colors, components and more.

Documentation is written in markdown and rendered with [Catalog](https://www.catalog.style/).
Catalog brings some special markdown features all of which are documented [here](https://docs.catalog.style/).

## License
[MIT License](LICENSE) IT-Gymnasiet Göteborg and Contributors.
