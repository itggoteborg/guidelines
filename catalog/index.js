import React from "react";
import ReactDOM from "react-dom";
import { Catalog, pageLoader } from "catalog";

const pages = [
	{
		path: "/",
		title: "Introduction",
		content: pageLoader(() => import("./pages/Introduction.md")),
	},
	{
		title: "Code style",
		pages: [
			{
				path: "/code-style/html",
				title: "HTML",
				content: pageLoader(() => import("./pages/CodeStyle/HTML.md")),
			},
			{
				path: "/code-style/css",
				title: "CSS",
				content: pageLoader(() => import("./pages/CodeStyle/CSS.md")),
			},
			{
				path: "/code-style/javascript",
				title: "JavaScript",
				content: pageLoader(() => import("./pages/CodeStyle/JS.md")),
			},
		],
	},
	{
		title: "Styleguide",
		pages: [
			{
				path: "/styleguide",
				title: "Overview",
				content: pageLoader(() => import("./pages/Styleguide/Overview.md")),
			},
			{
				path: "/styleguide/color",
				title: "Color",
				content: pageLoader(() => import("./pages/Styleguide/Color.md")),
			},
			{
				path: "/styleguide/typography",
				title: "Typography",
				content: pageLoader(() => import("./pages/Styleguide/Typography.md")),
			},
		],
	},
	{
		title: "Components",
		pages: [
			{
				path: "/components",
				title: "Overview",
				content: pageLoader(() => import("./pages/Components/Overview.md")),
			},
			{
				styles: [ "css/itg/button.css" ],
				path: "/components/button",
				title: "Button",
				content: pageLoader(() => import("./pages/Components/Button.md")),
			},
			{
				styles: [ "css/itg/card.css" ],
				path: "/components/card",
				title: "Card",
				content: pageLoader(() => import("./pages/Components/Card.md")),
			},
			{
				styles: [ "css/itg/fab.css" ],
				path: "/components/fab",
				title: "Floating action button",
				content: pageLoader(() => import("./pages/Components/FAB.md")),
			},
			{
				styles: [ "css/itg/input.css" ],
				path: "/components/input",
				title: "Input",
				content: pageLoader(() => import("./pages/Components/Input.md")),
			},
			{
				styles: [ "css/itg/textarea.css" ],
				path: "/components/textarea",
				title: "Textarea",
				content: pageLoader(() => import("./pages/Components/Textarea.md")),
			},
		],
	},
	{
		title: "Resources",
		pages: [
			{
				path: "/resources/logotypes",
				title: "Logotypes",
				content: pageLoader(() => import("./pages/Resources/Logotypes.md")),
			},
			{
				path: "/resources/code",
				title: "Code",
				content: pageLoader(() => import("./pages/Resources/Code.md")),
			},
		],
	},
];

const theme = {
	brandColor: "#212121",
	linkColor: "#EC0089",
	sidebarColorTextActive: "#F7922D",
	sidebarColorHeading: "#424242",
	lightColor: "#9E9E9E",
	pageHeadingBackground: "#009CDC",
};

ReactDOM.render(
	<Catalog title="IT-Gymnasiet Website Guidelines" pages={pages} logoSrc="itg-logo.svg" theme={theme} />,
	document.getElementById("catalog")
);
