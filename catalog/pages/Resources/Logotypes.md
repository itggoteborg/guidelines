# IT-Gymnasiet

## Göteborg
```image
{
    "span": 4,
    "light": true,
    "src": "itggot_logo.svg",
    "description": "IT-Gymnasiet Göteborg"
}
```

```download
{
    "span": 2,
    "url": "itggot_logo.svg",
    "title": "IT-Gymnasiet Göteborg Full logo",
    "subtitle": "11 KB"
}
```

# WE LOVE IT
```image
{
    "span": 2,
    "light": true,
    "src": "itg-logo.svg"
}
```

```download
{
    "span": 4,
    "url": "itg-logo.svg",
    "title": "IT-Gymnasiet WE LOVE IT logo",
    "subtitle": "11 KB"
}
```

## WE LOVE ...

<!-- GÖTEBORG -->
### GÖTEBORG
```image
{
    "span": 4,
    "src": "https://it-gymnasiet.se/wp-content/uploads/2017/04/startsida_we_love_goteborg_650x230.png"
}
```

```download
{
    "span": 2,
    "url": "https://it-gymnasiet.se/wp-content/uploads/2017/04/startsida_we_love_goteborg_650x230.png",
    "title": "WE LOVE GÖTEBORG.",
    "subtitle": "11 KB"
}
```

<!-- HELSINGBORG -->
### HELSINGBORG
```image
{
    "span": 4,
    "src": "https://it-gymnasiet.se/wp-content/uploads/2017/04/startsida_we_love_helsingborg_650x230.png"
}
```

```download
{
    "span": 2,
    "url": "https://it-gymnasiet.se/wp-content/uploads/2017/04/startsida_we_love_helsingborg_650x230.png",
    "title": "WE LOVE HELSINGBORG.",
    "subtitle": "11 KB"
}
```

<!-- KARLSTAD -->
### KARLSTAD
```image
{
    "span": 4,
    "src": "https://it-gymnasiet.se/wp-content/uploads/2017/04/startsida_we_love_karlstad_650x230.png"
}
```

```download
{
    "span": 2,
    "url": "https://it-gymnasiet.se/wp-content/uploads/2017/04/startsida_we_love_karlstad_650x230.png",
    "title": "WE LOVE KARLSTAD.",
    "subtitle": "11 KB"
}
```

<!-- KRISTANSTAD -->
### KRISTANSTAD
```image
{
    "span": 4,
    "src": "https://it-gymnasiet.se/wp-content/uploads/2017/04/startsida_we_love_kristianstad_650x230.png"
}
```

```download
{
    "span": 2,
    "url": "https://it-gymnasiet.se/wp-content/uploads/2017/04/startsida_we_love_kristianstad_650x230.png",
    "title": "WE LOVE KRISTANSTAD.",
    "subtitle": "11 KB"
}
```

<!-- SKÖVDE -->
### SKÖVDE
```image
{
    "span": 4,
    "src": "https://it-gymnasiet.se/wp-content/uploads/2017/04/startsida_we_love_skovde_650x230.png"
}
```

```download
{
    "span": 2,
    "url": "https://it-gymnasiet.se/wp-content/uploads/2017/04/startsida_we_love_skovde_650x230.png",
    "title": "WE LOVE SKÖVDE.",
    "subtitle": "11 KB"
}
```

<!-- STOCKHOLM -->
### STOCKHOLM
```image
{
    "span": 4,
    "src": "https://it-gymnasiet.se/wp-content/uploads/2017/04/startsida_we_love_stockholm_650x230.png"
}
```

```download
{
    "span": 2,
    "url": "https://it-gymnasiet.se/wp-content/uploads/2017/04/startsida_we_love_stockholm_650x230.png",
    "title": "WE LOVE STOCKHOLM.",
    "subtitle": "11 KB"
}
```

<!-- SUNDBYBERG -->
### SUNDBYBERG
```image
{
    "span": 4,
    "src": "https://it-gymnasiet.se/wp-content/uploads/2017/04/startsida_we_love_sundbyberg_650x230.png"
}
```

```download
{
    "span": 2,
    "url": "https://it-gymnasiet.se/wp-content/uploads/2017/04/startsida_we_love_sundbyberg_650x230.png",
    "title": "WE LOVE SUNDBYBERG.",
    "subtitle": "11 KB"
}
```

<!-- SÖDERTÖRN -->
### SÖDERTÖRN
```image
{
    "span": 4,
    "src": "https://it-gymnasiet.se/wp-content/uploads/2017/04/startsida_we_love_sodertorn_650x230.png"
}
```

```download
{
    "span": 2,
    "url": "https://it-gymnasiet.se/wp-content/uploads/2017/04/startsida_we_love_sodertorn_650x230.png",
    "title": "WE LOVE SÖDERTÖRN.",
    "subtitle": "11 KB"
}
```

<!-- UPPSALA -->
### UPPSALA
```image
{
    "span": 4,
    "src": "https://it-gymnasiet.se/wp-content/uploads/2017/04/startsida_we_love_uppsala_650x230.png"
}
```

```download
{
    "span": 2,
    "url": "https://it-gymnasiet.se/wp-content/uploads/2017/04/startsida_we_love_uppsala_650x230.png",
    "title": "WE LOVE UPPSALA.",
    "subtitle": "11 KB"
}
```

<!-- VÄSTERÅS -->
### VÄSTERÅS
```image
{
    "span": 4,
    "src": "https://it-gymnasiet.se/wp-content/uploads/2017/04/startsida_we_love_vasteras_650x230.png"
}
```

```download
{
    "span": 2,
    "url": "https://it-gymnasiet.se/wp-content/uploads/2017/04/startsida_we_love_vasteras_650x230.png",
    "title": "WE LOVE VÄSTERÅS.",
    "subtitle": "11 KB"
}
```

<!-- ÅKERSBERGA -->
### ÅKERSBERGA
```hint
IT-Gymnasiet Åkersberga will be decommissioned at the end of spring semester of 2018.
```

```image
{
    "span": 4,
    "src": "https://it-gymnasiet.se/wp-content/uploads/2017/04/startsida_we_love_akersberga_650x230.png"
}
```

```download
{
    "span": 2,
    "url": "https://it-gymnasiet.se/wp-content/uploads/2017/04/startsida_we_love_akersberga_650x230.png",
    "title": "WE LOVE ÅKERSBERGA.",
    "subtitle": "11 KB"
}
```

<!-- ÖREBRO -->
### ÖREBRO
```image
{
    "span": 4,
    "src": "https://it-gymnasiet.se/wp-content/uploads/2017/04/startsida_we_love_orebro_650x230.png"
}
```

```download
{
    "span": 2,
    "url": "https://it-gymnasiet.se/wp-content/uploads/2017/04/startsida_we_love_orebro_650x230.png",
    "title": "WE LOVE ÖREBRO.",
    "subtitle": "11 KB"
}
```
