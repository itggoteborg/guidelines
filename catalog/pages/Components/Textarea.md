## Light textarea

```html
{
    "dark": true
}
---
<textarea class="itg-textarea itg-textarea--light"></textarea>
```

```html
{
    "dark": true
}
---
<textarea class="itg-textarea itg-textarea--light itg-textarea--invalid"></textarea>
```

## Dark textarea

```html
{
    "light": true
}
---
<textarea class="itg-textarea itg-textarea--dark"></textarea>
```

```html
{
    "light": true
}
---
<textarea class="itg-textarea itg-textarea--dark itg-textarea--invalid"></textarea>
```
