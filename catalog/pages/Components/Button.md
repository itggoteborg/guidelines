## Light button

```html
{
    "span": 3,
    "dark": true,
    "plain": true
}
---
<button class="itg-button itg-button--light">&lt;button&gt; element</button>
```

```html
{
    "span": 3,
    "dark": true,
    "plain": true
}
---
<a href="" class="itg-button itg-button--light">&lt;a&gt; element</a>
```

## Dark button

```html
{
    "span": 3,
    "light": true,
    "plain": true
}
---
<button class="itg-button itg-button--dark">&lt;button&gt; element</button>
```

```html
{
    "span": 3,
    "light": true,
    "plain": true
}
---
<a href="" class="itg-button itg-button--dark">&lt;a&gt; element</a>
```
