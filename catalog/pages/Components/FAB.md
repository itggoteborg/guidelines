```html
{
    "light": true
}
---
<div class="itg-fab">
    <i class="itg-fab__icon">add</i>
</div>
```

```html
{
    "dark": true
}
---
<div class="itg-fab">
    <i class="itg-fab__icon">share</i>
</div>
```