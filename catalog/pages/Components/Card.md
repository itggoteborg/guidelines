```html
{
    "light": true
}
---
<div class="itg-card">
    <h3 class="itg-card__title">
        Card title
    </h3>
    <div class="itg-card__supporting-text">
        <p>
            Card supporting text
        </p>
    </div>
</div>
```

```html
{
    "dark": true
}
---
<div class="itg-card">
    <h3 class="itg-card__title">
        Card title
    </h3>
    <div class="itg-card__supporting-text">
        <p>
            Card supporting text
        </p>
    </div>
</div>
```