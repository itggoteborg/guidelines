## Light input

```html
{
    "dark": true
}
---
<input class="itg-input itg-input--light" placeholder="Light input">
```

```html
{
    "dark": true
}
---
<input class="itg-input itg-input--light itg-input--invalid" placeholder="Invalid input">
```

```html
{
    "dark": true
}
---
<input class="itg-input itg-input--light itg-input--half-width" placeholder="Half width input">
```

```html
{
    "dark": true
}
---
<input class="itg-input itg-input--light itg-input--full-width" placeholder="Full width input">
```

## Dark button

```html
{
    "light": true
}
---
<input class="itg-input itg-input--dark" placeholder="Dark input">
```

```html
{
    "light": true
}
---
<input class="itg-input itg-input--dark itg-input--invalid" placeholder="Invalid input">
```

```html
{
    "light": true
}
---
<input class="itg-input itg-input--dark itg-input--half-width" placeholder="Half width input">
```

```html
{
    "light": true
}
---
<input class="itg-input itg-input--dark itg-input--full-width" placeholder="Full width input">
```
