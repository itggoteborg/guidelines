## Brand colors

```color
{
    "span": 2,
    "name": "ITG Blue",
    "value": "#009CDC"
}
```

```color
{
    "span": 2,
    "name": "ITG Pink",
    "value": "#EC0089"
}
```

```color
{
    "span": 2,
    "name": "ITG Orange",
    "value": "#F7922D"
}
```

## Utility

```color
{
    "span": 1,
    "name": "Primary",
    "value": "#1E88E5"
}
```

```color
{
    "span": 1,
    "name": "Secondary",
    "value": "#9E9E9E"
}
```

```color
{
    "span": 1,
    "name": "Success",
    "value": "#43A047"
}
```

```color
{
    "span": 1,
    "name": "Danger",
    "value": "#F44336"
}
```

```color
{
    "span": 1,
    "name": "Warning",
    "value": "#FDD835"
}
```

```color
{
    "span": 1,
    "name": "Info",
    "value": "#26A69A"
}
```

```color
{
    "span": 1,
    "name": "Dark",
    "value": "#424242"
}
```

```color
{
    "span": 1,
    "name": "Light",
    "value": "#EEEEEE"
}
```

```color
{
    "span": 1,
    "name": "Black",
    "value": "#000000"
}
```

```color
{
    "span": 1,
    "name": "White",
    "value": "#FFFFFF"
}
```


## ITG Webtools

```color-palette
{
    "span": 2,
    "colors": [
        {
            "name": "Fast Forward",
            "value": "#FAD46B"
        },
        {
            "name": "Fast Forward dark",
            "value": "#C3A75A"
        },
        {
            "name": "Fast Forward foreground",
            "value": "#424242"
        }
    ]
}
```

```color-palette
{
    "span": 2,
    "colors": [
        {
            "name": "Smilie",
            "value": "#FF9933"
        },
        {
            "name": "Smilie dark",
            "value": "#B87A3D"
        },
        {
            "name": "Smilie foreground",
            "value": "#FFFFFF"
        }
    ]
}
```

```color-palette
{
    "span": 2,
    "colors": [
        {
            "name": "Selfie",
            "value": "#F84545"
        },
        {
            "name": "Selfie dark",
            "value": "#BA4444"
        },
        {
            "name": "Selfie foreground",
            "value": "#FFFFFF"
        }
    ]
}
```

```color-palette
{
    "span": 2,
    "colors": [
        {
            "name": "Ideamachine",
            "value": "#C2185B"
        },
        {
            "name": "Ideamachine dark",
            "value": "#793550"
        },
        {
            "name": "Ideamachine foreground",
            "value": "#FFFFFF"
        }
    ]
}
```

```color-palette
{
    "span": 2,
    "colors": [
        {
            "name": "Move IT",
            "value": "#7815D5"
        },
        {
            "name": "Move IT dark",
            "value": "#5F3784"
        },
        {
            "name": "Move IT foreground",
            "value": "#FFFFFF"
        }
    ]
}
```

```color-palette
{
    "span": 2,
    "colors": [
        {
            "name": "Groupie",
            "value": "#A88CD5"
        },
        {
            "name": "Groupie dark",
            "value": "#8773A8"
        },
        {
            "name": "Groupie foreground",
            "value": "#FFFFFF"
        }
    ]
}
```

```color-palette
{
    "span": 2,
    "colors": [
        {
            "name": "Helpie",
            "value": "#3D83B5"
        },
        {
            "name": "Helpie dark",
            "value": "#496579"
        },
        {
            "name": "Helpie foreground",
            "value": "#FFFFFF"
        }
    ]
}
```

```color-palette
{
    "span": 2,
    "colors": [
        {
            "name": "Handsup",
            "value": "#3EB5AC"
        },
        {
            "name": "Handsup dark",
            "value": "#497975"
        },
        {
            "name": "Handsup foreground",
            "value": "#FFFFFF"
        }
    ]
}
```

```color-palette
{
    "span": 2,
    "colors": [
        {
            "name": "Frankie",
            "value": "#4CAF50"
        },
        {
            "name": "Frankie dark",
            "value": "#517852"
        },
        {
            "name": "Frankie foreground",
            "value": "#FFFFFF"
        }
    ]
}
```
